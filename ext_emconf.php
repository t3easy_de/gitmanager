<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "gitmanager"
 *
 * Auto generated by Extension Builder 2013-06-03
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Git Manager',
	'description' => 'Manage Git repositories',
	'category' => 'plugin',
	'author' => 'Jan Kiesewetter',
	'author_email' => 'janYYYY@t3easy.de',
	'author_company' => 't3easy',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '',
	'constraints' => array(
		'depends' => array(
			'extbase' => '6.0',
			'fluid' => '6.0',
			'typo3' => '6.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>