<?php
namespace T3easy\Gitmanager\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package gitmanager
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ExtensionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * @var \T3easy\Gitmanager\Utility\ListUtility
	 */
	protected $listUtility;

	/**
	 * @param \T3easy\Gitmanager\Utility\ListUtility $listUtility
	 * @return void
	 */
	public function injectListUtility(\T3easy\Gitmanager\Utility\ListUtility $listUtility) {
		$this->listUtility = $listUtility;
	}

	/**
	 * extensionRepository
	 *
	 * @var \T3easy\Gitmanager\Domain\Repository\ExtensionRepository
	 * @inject
	 */
	protected $extensionRepository;

	/**
	 * @var \GitRepo
	 */
	protected $gitRepo;

	/**
	 * @param \GitRepo
	 */
	public function injectGit(\GitRepo $gitRepo) {
		$this->gitRepo = $gitRepo;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$availableGitExtensions = $this->listUtility->getAvailableGitExtensions();
		$availableAndInstalledGitExtensions = $this->listUtility->getAvailableAndInstalledExtensions($availableGitExtensions);
		$availableAndInstalledGitExtensions = $this->listUtility->enrichExtensionsWithEmConfAndTerInformation($availableAndInstalledGitExtensions);
		$this->view->assign('extensions', $availableAndInstalledGitExtensions);
	}

	/**
	 * action show
	 *
	 * @param \T3easy\Gitmanager\Domain\Model\Extension $extension
	 * @return void
	 */
	public function showAction(\T3easy\Gitmanager\Domain\Model\Extension $extension) {
		$this->view->assign('extension', $extension);
	}

	/**
	 * action new
	 *
	 * @param \T3easy\Gitmanager\Domain\Model\Extension $newExtension
	 * @dontvalidate $newExtension
	 * @return void
	 */
	public function newAction(\T3easy\Gitmanager\Domain\Model\Extension $newExtension = NULL) {
		$allowedInstallTypes = \TYPO3\CMS\Extensionmanager\Domain\Model\Extension::returnAllowedInstallTypes();
		foreach ($allowedInstallTypes as $allowedInstallType) {
			$localisedAllowedInstallTypes[$allowedInstallType] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
				'tx_gitmanager_domain_model_extension.installType.' . $allowedInstallType, 'gitmanager'
			);
		}
		$this->view->assign(
			'allowedInstallTypes',
			$localisedAllowedInstallTypes
		);
		$this->view->assign('newExtension', $newExtension);
	}

	/**
	 * action new
	 *
	 * @param \T3easy\Gitmanager\Domain\Model\Extension $newExtension
	 * @return void
	 */
	public function cloneAction(\T3easy\Gitmanager\Domain\Model\Extension $newExtension) {
		$paths = \TYPO3\CMS\Extensionmanager\Domain\Model\Extension::returnInstallPaths();
		$installType = $newExtension->getInstallType();
		$extensionPath = $paths[$installType] . $newExtension->getFolder();
		$this->gitRepo->create_new($extensionPath, $newExtension->getUrl());
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \T3easy\Gitmanager\Domain\Model\Extension $extension
	 * @return void
	 */
	public function editAction(\T3easy\Gitmanager\Domain\Model\Extension $extension) {
		$this->view->assign('extension', $extension);
	}

	/**
	 * action update
	 *
	 * @param \T3easy\Gitmanager\Domain\Model\Extension $extension
	 * @return void
	 */
	public function updateAction(\T3easy\Gitmanager\Domain\Model\Extension $extension) {
		$this->extensionRepository->update($extension);
		$this->flashMessageContainer->add('Your Extension was updated.');
		$this->redirect('list');
	}

}
?>